#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dynamic_array.h"

#define CMD_APPEND "append"
#define CMD_PREPEND "prepend"
#define CMD_INSERT "insert"
#define CMD_EXPAND "expand"
#define CMD_GET_LENGTH "get_length"
#define CMD_GET_VALUE "get_value"
#define CMD_CLEAR "clear"
#define CMD_POP_BACK "pop_back"
#define CMD_POP_FRONT "pop_front"
#define CMD_REMOVE "remove"
#define CMD_SHRINK "shrink"

static void print_array(Array *array)
{
	int64_t value;
	size_t length;

	fputs("[ ", stdout);

	length = array_get_length(array);

	for (size_t i = 0; i < length; i++) {
		array_get_value(array, i, &value);
		printf("%" PRId64 " ", value);
	}

	fputs("]\n", stdout);
}

int main(void)
{
	Array *array;
	char cmd[20];
	int n;
	int64_t value;
	size_t position, length;

	array = array_new();
	if (array == NULL) {
		fputs("Error: out-of-memory.\n", stderr);
		return EXIT_FAILURE;
	}

	while(true) {
		print_array(array);

		n = scanf("%19s", cmd);
		if (n < 1) break;

		if (strcmp(cmd, CMD_APPEND) == 0) {
			n = scanf("%" SCNd64, &value);

			if (n < 1)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_append(array, value))
				fputs("Append error\n", stdout);
		}
		else if (strcmp(cmd, CMD_PREPEND) == 0) {
			n = scanf("%" SCNd64, &value);

			if (n < 1)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_prepend(array, value))
				fputs("Prepend error\n", stdout);
		}
		else if (strcmp(cmd, CMD_INSERT) == 0) {
			n = scanf("%zu %" SCNd64, &position, &value);

			if (n < 2)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_insert(array, position, value))
				fputs("Insert error\n", stdout);
		}
		else if (strcmp(cmd, CMD_EXPAND) == 0) {
			n = scanf("%zu %" SCNd64, &length, &value);

			if (n < 2)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_expand(array, length, value))
				fputs("Expand error\n", stdout);
		}
		else if (strcmp(cmd, CMD_GET_LENGTH) == 0) {
			length = array_get_length(array);
			printf("%zu\n", length);
		}
		else if (strcmp(cmd, CMD_GET_VALUE) == 0) {
			n = scanf("%zu", &position);

			if (n < 1)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_get_value(array, position, &value))
				fputs("Get value error\n", stdout);
			else
				printf("%" PRId64 "\n", value);
		}
		else if (strcmp(cmd, CMD_CLEAR) == 0) {
			array_clear(array);
		}
		else if (strcmp(cmd, CMD_POP_BACK) == 0) {
			if (!array_pop_back(array, &value))
				fputs("Pop back error\n", stdout);
			else
				printf("%" PRId64 "\n", value);
		}
		else if (strcmp(cmd, CMD_POP_FRONT) == 0) {
			if (!array_pop_front(array, &value))
				fputs("Pop front error\n", stdout);
			else
				printf("%" PRId64 "\n", value);
		}
		else if (strcmp(cmd, CMD_REMOVE) == 0) {
			n = scanf("%zu", &position);

			if (n < 1)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_remove(array, position, &value))
				fputs("Remove error\n", stdout);
			else
				printf("%" PRId64 "\n", value);
		}
		else if (strcmp(cmd, CMD_SHRINK) == 0) {
			n = scanf("%zu", &length);

			if (n < 1)
				fputs("Error: invalid parameter\n", stdout);
			else if (!array_shrink(array, length))
				fputs("Shrink error\n", stdout);
		}
		else {
			fputs("Invalid command\n", stdout);
		}
	}

	array_delete(array);

	return EXIT_SUCCESS;
}
