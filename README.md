# dynamic\_array: exercise in C

An exercise in dynamic arrays. A program is used to intepret elementary operations on
dynamic arrays, like inserting and removing elements. The actual implementation of each
function of the dynamic array is empty and must be implemented by the user. The missing
functions are:

- allocate new empty dynamic array: `array_new()`
- deallocate dynamic array: `array_delete()`
- append value at the end of the array: `array_append()`
- prepend value at the beginning of the array:  `array_prepend()`
- insert value at specified position: `array_insert()`
- expand the array to the specified length: `array_expand()`
- return the array length: `array_get_length()`
- return a value from the array: `array_get_value()`
- empty the array: `array_clear()`
- pop and return last element of the array: `array_pop_back()`
- pop and return first element of the array: `array_pop_front()`
- remove and return element at specified position: `array_remove()`
- shrink the array to the specified length: `array_shrink()`

The user must implement these functions, then run the tests to check if they pass with the
provided implementation. When all tests pass, a pull request with the implementation
should be done to this repository.

### Requirements

- C compiler (ISO C 2011 compliant)
- meson build system
- ninja build system

Example Ubuntu requirements installation:

    $ sudo apt-get install gcc meson ninja-build

### Build instructions

    $ meson build
    $ ninja -C build

### Running instructions

    $ build/dynamic_array                  # run interactivelly
    $ build/dynamic_array < tests/input1   # run with some test input

### Test instructions

    $ ninja -C build test
